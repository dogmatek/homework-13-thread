package ru.sber.jd.task;


import ru.sber.jd.utils.CalculatorUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;


public class SumTask extends RecursiveTask<Integer> {
    private List<Integer> values;

    public SumTask(List<Integer> values) {
        this.values = values;
    }


    @Override
    protected Integer compute() {
        if (values.size() == 1) {
            return values.get(0);
        }

        List<Integer> a = new ArrayList<>();
        List<Integer> b = new ArrayList<>();

        int mid = values.size() / 2;
        for (int i = 0; i < mid; i++) {
            a.add(values.get(i));
        }

        for (int i = mid; i < values.size() ; i++) {
            b.add(values.get(i));
        }

        SumTask left = new SumTask(a);
        SumTask right = new SumTask(b);

        left.fork();
        right.fork();

        return CalculatorUtils.sum(right.join(), left.join());
    }
}
