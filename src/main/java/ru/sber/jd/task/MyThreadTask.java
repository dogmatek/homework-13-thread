package ru.sber.jd.task;

import ru.sber.jd.services.MyThread;
import ru.sber.jd.utils.CalculatorUtils;
import java.util.ArrayList;
import java.util.List;

public class MyThreadTask {
    Integer threadCount;


    public MyThreadTask(Integer threadCount)  {
        this.threadCount = threadCount;
    }

    public Integer calculate(List<Integer> values) throws InterruptedException {
        Integer sum = 0;


        List<List<Integer>> listValues = new ArrayList<>() ;
        // Разбиваем массив в двухмерный массив под каждый поток
        for (int j = 0; j < threadCount; j++) {
            listValues.add( new ArrayList<>());
        }

        for (int i = 0; i < values.size(); i++) {
            listValues.get(i%threadCount).add(i);
        }


        List<MyThread> threads = new ArrayList<>(); //создаем массив потоков


        for (int i = 0; i < threadCount; i++) {

            MyThread thread = new MyThread(listValues.get(i));
            threads.add(thread);
            thread.start();	        // Запуск потока

            sum = thread.getSum();  // Возвращаем сумму в потоке
        }

        for (int i = threads.size() - 1; i >= 0 ; i--) {
            threads.get(i).join();  // Ожидаем завершения потока
            sum = CalculatorUtils.sum(sum, threads.get(i).getSum());
        }
        return sum;
    }
}
