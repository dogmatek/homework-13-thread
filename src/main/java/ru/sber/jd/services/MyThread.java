package ru.sber.jd.services;

import ru.sber.jd.utils.CalculatorUtils;

import java.util.List;

public class MyThread extends Thread {
    private Thread thread;
    private Integer sum=0;
    private final List<Integer> values;

    public MyThread(List<Integer> values){
        this.values = values;
    }


    @Override
    public void run() {
        sum = 0; // На случай, если запуск происходит более 1 раза
        for (Integer value : this.values) {
            sum = CalculatorUtils.sum(sum, value);
        }
    }


    public Integer getSum(){
        return this.sum;
    }
}
