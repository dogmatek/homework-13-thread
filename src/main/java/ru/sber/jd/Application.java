package ru.sber.jd;

import ru.sber.jd.task.SumTask;
import ru.sber.jd.task.MyThreadTask;
import ru.sber.jd.utils.CalculatorUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class Application {
    public static void main(String[] args) throws InterruptedException {

        int countTread = 50; // Кол-во потоков
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", Integer.toString(countTread));

        List<Integer> values = new ArrayList<>();
        // Заполняем массив значениями
        for (int i = 0; i < 1000; i++) {
            values.add(i);
        }




        // Вычисление тремя методами
        for (int i = 0; i < 3; i++) {
            String method = "";
            Integer result = 0;
            //Начинаем замер
            long startTime = System.currentTimeMillis();
            switch (i){
                case 0:
                    method="parallelStream";
                    // Указваем кол-во потоков
                    result = values.parallelStream().reduce(CalculatorUtils::sum).get();
                    break;
                case 1:
                    method="ForkJoinPull";
                    result = new ForkJoinPool(countTread).invoke( new SumTask(values));
                    break;
                case 2:
                    method="Thread";
                    //Создание потока
                    MyThreadTask myThreadTask = new MyThreadTask(countTread);
                    result = myThreadTask.calculate(values);
                    break;
                default:
            }
            //Заканчиваем замер
            long endTime = System.currentTimeMillis();

            // Выводим результат
            System.out.printf("Vетод = %s, Результат = %s, Время работы = %s\n", method, result, (endTime - startTime));
        }
    }
}
